# Introducción al _software_ libre para editores

* Horas: 3
* Tipo: teórico-práctico
* Relaciones: 8e, 9e, 12e, 17e, 22e

## Descripción

Las metodologías más populares en la edición contemporánea han
sido acaparadas por dos grandes empresas y sus paqueterías de
_software_: Microsoft Office y Adobe Creative Cloud. Esto representa
una serie de problemas de índole económica, social, política
así como de producción y reproducción de la cultura y del conocimiento.
En este módulo se verán las posibilidades, límites, oportunidades
y riesgos de esta dependencia tecnológica o de sus alternativas:
el _software_ libre o el código abierto. Una vez expuesta la
problemática entre el _software_ libre y propietario en la edición,
se instalarán alternativas gratuitas, abiertas o libres de los
programas vendidos por Microsoft o Adobe.

## Objetivos

* Comprender las posibilidades y los límites del uso de tecnologías
  propietarias.
* Aprender sobre las oportunidades y los riesgos del _software_ libre
  o del código abierto.
* Instalar programas alternativos a los ofrecidos por Microsoft
  o Adobe.

## Temas

1. Adobe y Microsoft, los grandes hermanos.
2. Lo gratuito, lo abierto y lo libre.
3. ¿Qué es el _software_ libre y qué es el código abierto (+++FOSS+++)?
4. +++FOSS+++ en la edición, ¿para qué?
5. Herramientas editoriales: Scribus, TeX, Pecas y más.
6. Herramientas de diseño: Gimp, Inkscape y Krita.
7. Herramientas de comunicación y gestión: Telegram, pads y
   Nextcloud.
8. Herramientas de digitalización: LibreScan, ScanTailor y Tesseract.
9. Servidores de correo alternativos: Riseup y Disroot.
10. Plataformas de difusión: las redes federadas.
11. Plataformas de distribución: LibGen, Aaaaarg y Leanpub.
12. Giro completo en el +++FOSS+++: alternativas a Windows y macOS.
13. Algunas navajas suizas: Pandoc, Git, +++SSH+++, Xpdf y `libtiff`.

## Conocimientos previos

* Usar o tener nociones básicas del _software_ ofrecido por Microsoft
  o Adobe.

## Materiales necesarios

* Computadoras para instalar los programas.
* Conexión a internet.
* Cañón.
