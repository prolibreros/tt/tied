# Fundamentos de programación con Ruby

* Horas: 20
* Tipo: práctico
* Relaciones: 5r, 6r, 8e, 9e, 18n, 20e

## Descripción

«La edición se hizo código». Esta frase, aunque abrupta, sintetiza
una cuestión rara vez visible en los procesos editoriales: no
importan los formatos empleados para la producción de publicaciones
ni los programas usados para generarlos, al final todo se reduce
a ceros y unos. Con el uso de Ruby, un lenguaje de programación,
se verán los fundamentos para empezar a desarrollar rutinas mediante
_scripts_ que permitan la simplificación y la automatización
de tareas monótonas llevadas a cabo en la edición.

## Objetivos

* Instalar y configurar Ruby.
* Aprender los fundamentos de programación a través de Ruby.
* Programar _scripts_ para automatizar el procesamiento de texto.
* Usar _scripts_ como programas de terminal.

## Temas

1. ¿Qué son los lenguajes de programación y Ruby?
2. Instalación y configuración de Ruby.
3. Números, líneas de textos y variables.
4. Conversión de variables y algunos métodos de Ruby.
5. Condicionales.
6. Matrices e iteraciones.
7. Clase `Hash` de Ruby.
8. Creación de métodos.
9. Extensión de clases.
10. Uso de gemas y de otras herramientas en Ruby.
11. Formateo de texto.
13. Conversión de formatos de texto.
14. Desarrollo de _scripts_ de automatización.
15. De _script_ a programa de la terminal.

## Conocimientos previos

* Uso de la terminal Bash.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
