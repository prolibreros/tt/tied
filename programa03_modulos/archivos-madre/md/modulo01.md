# Metodología de la edición ramificada

* Horas: 3
* Tipo: teórico
* Relaciones: 2e, 5e, 6e, 8e

## Descripción

Este módulo consistirá en explicar en qué consiste la «edición
ramificada» y cómo funciona para producir de manera automatizada,
y en segundos, diversos formatos ---como +++PDF+++, +++EPUB+++
y +++MOBI+++--- a partir de unos «archivos madre». En contraste
con la «edición tradicional» o «cíclica», en donde cada nuevo
formato requiere de uno previo para repetir varios de los procesos
habituales en la producción de publicaciones, la edición ramificada
realiza procesos independientes para cada uno de los formatos
finales. Las ventajas que esto conlleva son la mejora en la calidad
editorial y técnica de las publicaciones, un uso más eficiente
de los recursos y un menor tiempo de producción en comparación
con otros métodos empleados en la edición.

## Objetivos

* Aprender la distinción entre la publicación estandarizada y la
  no-estandarizada.
* Conocer qué son y en qué se diferencian la edición tradicional,
  la cíclica y la ramificada.
* Reflexionar sobre los diez puntos metodológicos de la edición
  ramificada.
* Describir en qué consisten los archivos madres y qué tipo de
  formatos se utilizan para ellos.
* Dilucidar las posibilidades, los límites, las ventajas y las
  desventajas de la edición ramificada.

## Temas

1. Supuestos básicos de la edición ramificada.
2. Tipos de edición digital.
3. Tipos de edición digital estandarizada: tradicional, cíclica y
   ramificada.
4. Aspectos generales de la edición ramificada.
5. Diez puntos metodológicos de la edición ramificada.
6. El papel central de los archivos madre.
7. Formatos y sintaxis.
8. Ejemplos de la metodología ramificada.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadora con acceso a internet para ver las diapositivas.
