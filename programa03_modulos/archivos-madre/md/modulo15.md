# Licencias de uso para publicaciones

* Horas: 5
* Tipo: teórico
* Relaciones: 13e, 14e, 16e

## Descripción

Las licencias de uso son documentos que establecen la posibilidad
de reproducción de una obra como su copia, traducción o adaptación.
Este contrato «resguarda» la integridad total o parcial del producto
cultural, ya sea una pieza musical, artística, literaria o de
_software_. En este módulo se reflexionará sobre las licencias
de uso más populares y cuales son las ventajas y desventajas
de sus aplicaciones.

## Objetivos

* Conocer los diferentes tipos de licencias.
* Explicar los componentes de una licencia.
* Comprender cuál es la importancia de su uso.
* Reflexionar por qué se siguen utilizando y sus implicaciones. 

## Temas

1. ¿Qué es una licencia de uso?
2. ¿Qué es el _copyright_ y cómo funciona?
3. Otras opciones: Creative Commons, _copyfarleft_, Licencia de pares.
5. +++LEAL+++, una alternativa editorial radical.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Cañón.
