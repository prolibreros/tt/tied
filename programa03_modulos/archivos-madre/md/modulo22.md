# Respositorios Git: respaldo de información

* Horas: 15
* Tipo: teórico-práctico
* Relaciones: 5e, 6e, 7e, 8e, 10e, 11e, 12e, 18n

## Descripción

El respaldo de la información siempre ha sido un dolor de cabeza
para los editores. Con mil y un discos duros se cree tener una
solución, pero no son confiables cuando son demasiados colaboradores.
Una solución es Git, un programa de código abierto que realiza
un seguimiento preciso de los cambios en un proyecto, más conocido
en tecnologías de la información como un controlador de versiones.
Este módulo brindará las herramientas y los conocimientos necesarios
para utilizar repositorios Git en lugar de discos duros, nubes
---como Google Drive o Dropbox--- o sin fines de versiones últimas
de cada proyecto editorial. Se enseñará la configuración de un
repositorio y su mantenimiento, la subida y descarga de información
y la adición de colaboradores: todo de manera gratuita.

## Objetivos

* Conocer el esquema de trabajo de Git.
* Aprender a crear y clonar repositorios.
* Explicar los comandos básicos de Git.
* Reflexionar sobre las ventajas de este modelo de trabajo.

## Temas

1. ¿Qué es un control de versiones?
2. ¿Cómo crear mi primer repositorio? 
3. Comandos básicos de Git.
4. Ramas, _branches_ y _forks_.
5. Cómo bajar, subir y comentar información.
6. Ejemplos de proyectos editoriales que usan Git.

## Conocimientos previos

* Uso básico de la terminal.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
