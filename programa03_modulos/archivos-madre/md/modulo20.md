# Fundamentos de RegEx

* Horas: 10 
* Tipo: práctico
* Relaciones: 2r, 5e, 8e, 9e, 19e

## Descripción

Aquí el texto es una cadena de caracteres. RegEx es un lenguaje
y una herramienta muy poderosa que se puede implementar en la
edición si se le usa de manera adecuada. Las expresiones regulares
pueden ser muy útiles para extraer información a través de patrones.
En este módulo se aprenderá a limpiar un texto y a darle la estructura
correcta. Se mostrarán fórmulas básicas que ayuden a sustituir
información y a entender que el «buscar» y «reemplazar» puede
ofrecer una solución confiable si se tienen las bases para utilizarlos.

## Objetivos

* Conocer qué son las expresiones regulares. 
* Explicar la nomenclatura de RegEx.
* Describir fórmulas para la limpieza de formato.

## Temas

1. Todo esencialmente es un carácter.
2. ¿Qué significa cada carácter en RegEx?
3. Buscar y reemplazar todo.
4. Fórmulas para limpiar y formatear texto. 

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
