# Scribus y tipografías libres I

* Horas: 10
* Tipo: teórico-práctico
* Relaciones: 4e, 11e, 22e

## Descripción

No todo es InDesign. En el enfoque del _desktop publishing_ también
existe una alternativa gratuita y libre para la maquetación de
textos: Scribus. En este módulo se describirán las ventajas y
desventajas de su uso para la producción de publicaciones en
relación con InDesign. Una parte importante de este enfoque es
el empleo diverso de fuentes, así que también se abarcará la
problemática entre las tipografías de pago y las libres. A través
de ejercicios simples se enseñará a instalar Scribus, a buscar
fuentes libres y a producir publicaciones de alta calidad sin
la necesidad de amplios recursos económicos o sin el ejercicio
de la piratería, al mismo tiempo que se ahorra espacio en el
disco duro y consumo de memoria +++RAM+++.

## Objetivos

* Instalar Scribus.
* Aprender a configurar un documento en Scribus.
* Conocer qué son las páginas maestras.
* Maquetar un texto con Scribus.
* Comprender qué son las tipografías libres.
* Buscar e instalar tipografías libres.
* Usar tipografías libres en Scribus.
* Producir un +++PDF+++.

## Temas

1. ¿Qué es Scribus, la alternativa libre a InDesign?
2. Configuración y entorno de trabajo de Scribus.
3. ¿Qué son las tipografías libres?
4. Instalación y uso de tipografías libres.
5. Diseño de páginas maestras en Scribus.
6. Maquetación de texto en Scribus.
7. Producción de +++PDF+++ con Scribus.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para descargar e instalar Scribus.
* Conexión a internet.
* Cañón.
