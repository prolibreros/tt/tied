# Conversión de documentos: +++MD, DOCX, HTML+++ y más

* Horas: 3
* Tipo: práctico
* Relaciones: 1e, 2e, 3e, 4e, 6r, 8e, 18n, 20e, 22e

## Descripción

Hoy en día la conversión de documentos nos ahorra tiempo y esfuerzo.
Aquí se aprenderá a utilizar esta herramienta para pasar de un
formato a otro y obtener un mayor control sobre nuestro texto,
sin necesidad de tener conocimientos profundos sobre lenguajes
de marcado. La conversión de documentos se vuelve indispensable
cuando estamos hablando de un flujo de trabajo constante en el
que la prioridad es la agilización para la producción de múltiples
formatos.

## Objetivos

* Hacer la instalación de Pandoc y Pecas.
* Conocer los fundamentos básicos de Pandoc.
* Saber cuáles son los parámetros necesarios para la conversión. 
* Reflexionar sobre las características de cada archivo y detectar cuál es el más propicio dependiendo del proyecto editorial. 

## Temas

1. ¿Qué es Pandoc?
2. ¿Por qué Pandog y no Pandoc?
3. Parámetros necesarios para la conversión.
4. Ejemplos de conversión de archivos.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
