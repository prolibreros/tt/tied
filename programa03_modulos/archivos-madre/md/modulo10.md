# Flujo para la gestión de traducciones

* Horas: 10
* Tipo: teórico-práctico
* Relaciones: 2e, 11r, 18n, 19r, 22e

## Descripción

La traducción de textos puede ser un proceso tortuoso y de creciente
complejidad según la cantidad de idiomas a traducir. Sin embargo,
a través del uso de `gettext`, la biblioteca +++GNU+++ de internacionalización,
es posible generar archivos que faciliten la traducción. En este
módulo se explicará el proceso técnico para pasar de archivos
de procesamiento de texto a los formatos +++PO+++ necesarios
para la gestión y la coordinación de proyectos de traducción.
También se esclarecerán las ventajas de este flujo de trabajo
como son la traducción simultánea, la posibilidad de trabajo
en línea a través de Weblate y la capacidad de incorporar las
traducciones a diversos formatos de salida como son +++HTML+++,
+++MD+++ o documentos de texto procesado.

## Objetivos

* Comprender la importancia de `gettext` y los archivos +++PO+++
  para la gestión de traducciones.
* Aprender la dinámica de trabajo de +++MD+++ a +++PO+++ para
  concluir en archivos +++MD+++, +++HTML+++, +++TEX+++ o +++PDF+++.
* Instalar Poedit.
* Usar la plataforma Weblate.
* Realizar ejercicios de traducción para poner en práctica la
  metodología.

## Temas

1. ¿Qué es `gettext` y los archivos +++PO+++?
2. Instalación de Poedit.
3. Suscripción a la plataforma Weblate.
4. Estructuración del texto a Markdown.
5. Conversión de +++MD+++ a archivos +++PO+++.
6. Traducción a partir de archivos +++PO+++ usando Poedit o Weblate.
7. Conversión de archivos +++PO+++ a +++MD+++, +++HTML+++, +++TEX+++
   o +++PDF+++.

## Conocimientos previos

* Uso de la terminal Bash.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
