# Instalación virtual de +++GNU+++/Linux

* Horas: 2
* Tipo: práctico
* Relaciones: 11e

## Descripción

El uso de +++GNU+++/Linux hoy en día es muy sencillo. Sin embargo,
por diversos motivos no es fácil la decisión de instalar un nuevo
sistema operativo. Debido a fines pedagógicos a veces la mejor
opción es el uso de un mismo entorno. En este módulo se instalará
una distribución virtual de +++GNU+++/Linux sobre el sistema
operativo de base; tan simple como instalar otro programa en
computadoras con Windows o macOS. La ventaja es el acceso a las
herramientas utilizadas en esta currícula para que el usuario
se enfoque en los objetivos de aprendizaje y en el descubrimiento
de las ventajas que ofrece +++GNU+++/Linux.

## Objetivos

* Instalar VirtualBox en cualquier sistema operativo, incluyendo
  Windows o macOS.
* Instalar mediante VirtualBox un clon de Debian ---una distribución
  de +++GNU+++/Linux--- que incluye las configuraciones y las
  herramientas necesarias para esta currícula.
* Instalar las _guest additions_ para aumentar la usabilidad, como
  es la posibilidad de carpetas compartidas.
* Entender de manera somera la dinámica _guest/host_: cómo se
  relaciona el sistema operativo virtual con el sistema operativo
  de base.

## Temas

1. ¿Qué es una máquina virtual y qué es VirtualBox?
2. ¿Qué es una distribución de +++GNU+++/Linux y Debian?
3. ¿Qué son las máquinas _guest_ y _host_?
4. Descarga e instalación de VirtualBox.
5. Descarga e instalación de un clon de Debian.
6. Descarga e instalación de _guest additions_.
7. Configuración de carpetas compartidas.

## Conocimientos previos

* Conocimiento de descarga e instalación de programas en el sistema
  operativo en uso.

## Materiales necesarios

* Una computadora con al menos 6 +++GB+++ de espacio.
* Una conexión a internet.
