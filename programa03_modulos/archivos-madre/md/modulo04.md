# Fundamentos de TeX

* Horas: 10
* Tipo: teórico-práctico
* Relaciones: 2e, 2r, 3e, 5e, 5r, 6e, 8e, 12e, 17r

## Descripción

En el mundo de la edición existen dos grandes metodologías para
la producción de publicaciones impresas. La más popular es el
_desktop publishing_ cuyo ejemplo son InDesign o Scribus. Pese
a su relativa corta curva de aprendizaje, este tipo de método
se vuelve problemático cuando se trata de automatizar o procesar
grandes volúmenes de texto. Desde los ochenta existe una solución
a esta dificultad: los sistemas de composición tipográfica. En
este módulo se verán los fundamentos de TeX, el sistema de composición
más robusto y popular. A partir del uso de macros, se explicará
y aplicará la estructura de un documento TeX. Además se describirán
el proceso y las herramientas necesarias para producir +++PDF+++
con este sistema de composición.

## Objetivos

* Aprender las diferencias entre el _desktop publishing_ y los
  sistemas de composición tipográfica.
* Comprender qué son los macros de LaTeX.
* Configurar un documento básico de LaTeX.
* Modificar diseño de las plantillas por defecto de LaTeX.

## Temas

1. Ventajas y desventajas del _desktop publishing_.
2. ¿Qué es un sistema de composición tipográfica?
3. ¿Qué es TeX?
4. ¿Qué es LaTeX?
5. Configuración de un documento con TeXstudio.
6. Modificaciones del diseño por defecto de LaTeX.
7. Búsqueda e implementación de plantillas.
8. Generación de +++PDF+++ a través de `lualatex`.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
