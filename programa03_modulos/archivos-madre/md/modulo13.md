# Propiedad intelectual y derechos de autor

* Horas: 10
* Tipo: teórico
* Relaciones: 14e, 15e, 16e

## Descripción

En el campo de la producción cultural contemporánea se tiene
un conjunto de legislaciones cuyo discurso busca proteger la
producción como propiedad. Por ejemplo, las patentes, las marcas,
el diseño industrial, las denominaciones de origen y, por supuesto,
los derechos de autor. Pese a su complejidad jurídica y paulatino
robustecimiento cuantitativo y cualitativo, estas legislaciones
---que en conjunto se conocen como «propiedad intelectual»---
no tienen teorías congruentes que las justifiquen. En este módulo
se analizarán las propuestas teóricas para la protección de este
régimen de propiedad, con especial énfasis en los derechos de
autor.

## Objetivos

* Conocer qué se entiende por «propiedad intelectual».
* Aprender las diferencias entre los derechos de autor y el
  _copyright_.
* Estudiar la teoría progresista, personalista y laborista.
* Reflexionar sobre los supuestos y los límites de la propiedad
  intelectual.

## Temas

1. Morfología de la propiedad intelectual.
2. En la búsqueda de una definición y una teoría de la propiedad
   intelectual.
3. La teoría progresista: el utilitarismo de la legislación
   estadunidense.
4. La teoría personalista: Hegel, Kant y ¿más Hegel?
5. La teoría laborista: la propiedad de Locke.
6. Vertientes continental y anglosajona de la propiedad literaria:
   los derechos de autor y el _copyright_.
7. ¿La producción es propiedad?: crítica de Proudhon a la propiedad
   literaria.
8. La producción cultural y la reproducción de capital: la
   crítica de Walter Benjamin al autor y la obra.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Ninguno.
