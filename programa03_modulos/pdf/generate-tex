#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'fileutils'

$headers = <<-HEREDOC
\\chapter{@chapter}

\\pagestyle{fancy}
\\fancyhead{}
\\fancyhead[CO]{\\footnotesize\\textsc{@chapter}\\normalsize}
\\fancyhead[CE]{\\footnotesize\\textsc{\\myauthor}\\normalsize}

\\vskip 3em
HEREDOC

$modules = []
$summary = []
$titles  = []

# Modifica tildes y esas cosas para TeX
def tex_transliterate s
  chars   = s.split('')
  change  = []
  bib     = [
    ['á', "@'{a}"],['é', "@'{e}"],['í', "@'{i}"],
    ['ó', "@'{o}"],['ú', "@'{u}"],['ü', "@\"{u}"],
    ['ñ', "@~{n}"],['Á', "@'{A}"],['É', "@'{E}"],
    ['Í', "@'{I}"],['Ó', "@'{O}"],['Ú', "@'{U}"],
    ['Ü', "@\"{U}"],['Ñ', "@~{N}"]
  ]

  chars.each do |c|
    bib.each do |x|
      if c == x.first
        c = x.last
      end
    end

    change.push(c)
  end

  return change.join('')
end

# Genera la estructura para los gráficos
def generate_graphics s
  header = "\\begin{figure}[ht]\n\\centering\n  \\includegraphics[height=\\textheight,keepaspectratio]{"
  footer = "}\n\\end{figure}"
  return header + s + footer
end

# Va al directorio 'tex'
Dir.chdir(__dir__)

# Iteración de cada archivo
Dir.glob('../archivos-madre/md/*.md') do |f|
  # Convierte el archivo a .tex
  system("pc-pandog -i #{f} -o #{__dir__}/src/#{File.basename(f, '.*')}.tex")

  # Obtiene el texto procesado por pc-pandog
  raw   = File.read("src/#{File.basename(f, '.*')}.tex")

  # Obtiene el título limpio
  regex = /^(.|\n)*?\\section{((.|\n)*?)}\\label(.|\n)*$/
  title = raw.gsub(regex, '\2')
             .gsub(/\n/, ' ')
             .gsub(/\\texorpdfstring{(.*?)}{.*}/, '\1')
  new_t = $headers.gsub('@chapter', title)
                  .gsub('@tex_chapter', tex_transliterate(title))

  # Reemplaza el título y limpia otras cosillas
  regex = /^(.|\n)*?\\section{((.|\n)*?)}\\label{(\S*|\n)}/
  clean = raw.gsub(regex, new_t)
             .gsub(/\+\+\+(.*?)\+\+\+/,){|e| e.downcase }
             .gsub(/\+\+\+(.*?)\+\+\+/, '\\textsc{' + '\1' + '}')
             .gsub(/\\hypertarget{.*?%/, '')
             .gsub(/\\label{.*?}}/, '')
             .gsub('subsection', 'subsection*')
             .gsub("\\tightlist\n", '')
             .gsub("\\def\\labelenumi{\\arabic{enumi}.}\n", '')
             .gsub('begin{itemize}', 'begin{itemize}[noitemsep]')
             .gsub('begin{enumerate}', 'begin{enumerate}[noitemsep]')
             .gsub(/\\includegraphics{(.*?)}/, generate_graphics('\1'))
             .gsub('LaTeX', '\\LaTeX')
             .gsub('TeX', '\\TeX')
             .gsub('@', '\\')
             .gsub("\n\n\n", "\n\n")

  # Guardado
  file = File.open("src/#{File.basename(f, '.*')}.tex", 'w:utf-8')
  file.puts clean
  file.close
end

# Obtiene la ruta a los archivos TeX de los módulos
Dir.glob('src/*.tex') do |f|
  if f =~ /\d+/
    $modules.push(f)
  end
end

# Extrae el título y las descripciones de los módulos
$modules.sort!
$modules.each_with_index do |f,i|
  raw   = File.read(f)
  title = raw.gsub(/^(.|\n)*?chapter{(.*?)}\n(.|\n)*$/, '\2')
  text  = raw.gsub(/^(.|\n)*subsection\*{Descripción}\s+(\S(.|\n)*)\s+\\subsection\*{Objetivos}(.|\n)*$/, '\2')
  final = (i + 1).to_s + ". " + title

  $summary.push("\\subsection*{" + (i + 1).to_s + ". " + title + "}\n\n")
  $summary.push(text + "\n")
  $titles.push("\n\\item\n  " + title)
end

# Guardado del archivo sumario.tex
file = File.open('src/sumario.tex', 'w:utf-8')
file.puts "\\chapter{Sumario}\n\n"
file.puts "\\pagestyle{fancy}"
file.puts "\\fancyhead{}"
file.puts "\\fancyhead[CO]{\\footnotesize\\textsc{Sumario}\\normalsize}"
file.puts "\\fancyhead[CE]{\\footnotesize\\textsc{\\myauthor}\\normalsize}\n\n"
file.puts "\\vskip 3em\n\n"
file.puts $summary
file.close

# Modifica la lista de módulos del mapa
mapa = File.read('src/mapa.tex')
mapa = mapa.gsub('\\modules', "\\begin{enumerate}[noitemsep]" + $titles.join('') + "\n\\end{enumerate}")
file = File.open('src/mapa.tex', 'w:utf-8')
file.puts mapa
file.close

