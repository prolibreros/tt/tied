\chapter{Sumario}

\pagestyle{fancy}
\fancyhead{}
\fancyhead[CO]{\footnotesize\textsc{Sumario}\normalsize}
\fancyhead[CE]{\footnotesize\textsc{\myauthor}\normalsize}

\vskip 3em

\subsection*{1. Metodología de la edición ramificada}

Este módulo consistirá en explicar en qué consiste la «edición
ramificada» y cómo funciona para producir de manera automatizada, y en
segundos, diversos formatos ---como \textsc{pdf}, \textsc{epub} y
\textsc{mobi}--- a partir de unos «archivos madre». En contraste con la
«edición tradicional» o «cíclica», en donde cada nuevo formato requiere
de uno previo para repetir varios de los procesos habituales en la
producción de publicaciones, la edición ramificada realiza procesos
independientes para cada uno de los formatos finales. Las ventajas que
esto conlleva son la mejora en la calidad editorial y técnica de las
publicaciones, un uso más eficiente de los recursos y un menor tiempo de
producción en comparación con otros métodos empleados en la edición.

\subsection*{2. Lenguajes de marcado: \textsc{md} y \textsc{html}}

Un lenguaje de marcado es una manera de darle estructura a un texto. En
este módulo aprenderemos a aplicar este tipo de sintaxis a las
publicaciones para que sean fáciles de leer y entendibles para cualquier
computadora. Para ello, se conocerán las ventajas y desventajas de
trabajar con Markdown y \textsc{html}, se pondrán a prueba dependiendo de
cada una de las necesidades del proyecto editorial y se acostumbrará a
trabajar las estructuras antes que el diseño.

\subsection*{3. Artículos académicos en \textsc{xml jats} I}

Hoy en día para la publicación de artículos académicos en repositorios
científicos es necesario producir archivos en formato \textsc{xml} y
\textsc{pdf}. Uno de los esquemas más populares es \textsc{jats}, el cual con
ciertas modificaciones es usado por Sci\textsc{elo}. En este módulo se
explicará la importancia de este tipo de formatos y su esquema básico,
así como las herramientas y las metodologías que pueden emplearse para
evitar hacer un doble trabajo ---la estructuración del \textsc{xml} y la
maquetación del \textsc{pdf}---. Con un enfoque de lo simple a lo complejo,
se mostrará cómo es posible pasar de lenguajes de marcado ligero, como
Markdown, a archivos \textsc{xml} y, de ahí, a su importación para la
producción de \textsc{pdf}.

\subsection*{4. Fundamentos de \TeX}

En el mundo de la edición existen dos grandes metodologías para la
producción de publicaciones impresas. La más popular es el \emph{desktop
publishing} cuyo ejemplo son InDesign o Scribus. Pese a su relativa
corta curva de aprendizaje, este tipo de método se vuelve problemático
cuando se trata de automatizar o procesar grandes volúmenes de texto.
Desde los ochenta existe una solución a esta dificultad: los sistemas de
composición tipográfica. En este módulo se verán los fundamentos de \TeX,
el sistema de composición más robusto y popular. A partir del uso de
macros, se explicará y aplicará la estructura de un documento \TeX.
Además se describirán el proceso y las herramientas necesarias para
producir \textsc{pdf} con este sistema de composición.

\subsection*{5. Conversión de documentos: \textsc{md, docx, html} y más}

Hoy en día la conversión de documentos nos ahorra tiempo y esfuerzo.
Aquí se aprenderá a utilizar esta herramienta para pasar de un formato a
otro y obtener un mayor control sobre nuestro texto, sin necesidad de
tener conocimientos profundos sobre lenguajes de marcado. La conversión
de documentos se vuelve indispensable cuando estamos hablando de un
flujo de trabajo constante en el que la prioridad es la agilización para
la producción de múltiples formatos.

\subsection*{6. Pecas: herramientas editoriales desde la terminal}

Hacer un \textsc{epub} nunca había sido tan fácil como lo es ahora. En un
entorno técnico y según el método ramificado de edición es como surgió
Pecas: una serie de \emph{scripts} que automatizan el quehacer editorial
y que pueden marcar una diferencia en su calidad. En este modulo se
conocerá a fondo la utilización de esta herramienta y el uso de sus
parámetros según las necesidades de cada proyecto editorial.

\subsection*{7. Digitalización: del impreso al digital}

Este módulo se enfocará en el traslado del impreso al digital. Se
conocerán los pasos necesarios que nos facilitarán el trabajo de la
extracción del texto y la obtención de un \textsc{pdf} a partir del
levantamiento de imágenes hechas con un escáner. Además, se conocerán
diferentes tipos de escáneres, el \emph{software} necesario para el
posprocesamiento y las medidas de calidad pertinentes para la
adquisición de un producto final.

\subsection*{8. Mundo de los \emph{ebooks}}

Un \emph{ebook} y un \textsc{epub} no son lo mismo aunque comparten una
relación en común: la producción de una publicación electrónica. Un
\textsc{epub} es un conjunto de documentos \textsc{xhtml} comprimidos en un
archivo \textsc{zip} para su portabilidad y su legibilidad. En este módulo
se hará un análisis teórico sobre el desarrollo de las publicaciones
estandarizadas y propietarias, como los formatos de Amazon o de Apple.

\subsection*{9. Mundo de los \emph{appbooks}}

Con el advenimiento y desarrollo de las tecnologías \emph{web} nos
encontramos en un mundo diferente con respecto a la visualización y el
manejo de la información. En este contexto es donde nacen las
publicaciones no estandarizadas. Las aplicaciones y los \emph{appbooks}
son producciones digitales que se complementan con el usuario a través
de una dinámica interactiva. Estas publicaciones se basan en distintas
tecnologías para ofrecernos la posibilidad de desarrollar objetos
digitales que brindan narrativas poco convencionales y permiten nuevas
experiencias de lectura. En este módulo se describirán los tipos de
publicación no estandarizada, su funcionamiento y las herramientas más
utilizadas para su elaboración.

\subsection*{10. Flujo para la gestión de traducciones}

La traducción de textos puede ser un proceso tortuoso y de creciente
complejidad según la cantidad de idiomas a traducir. Sin embargo, a
través del uso de \texttt{gettext}, la biblioteca \textsc{gnu} de
internacionalización, es posible generar archivos que faciliten la
traducción. En este módulo se explicará el proceso técnico para pasar de
archivos de procesamiento de texto a los formatos \textsc{po} necesarios
para la gestión y la coordinación de proyectos de traducción. También se
esclarecerán las ventajas de este flujo de trabajo como son la
traducción simultánea, la posibilidad de trabajo en línea a través de
Weblate y la capacidad de incorporar las traducciones a diversos
formatos de salida como son \textsc{html}, \textsc{md} o documentos de texto
procesado.

\subsection*{11. Introducción al \emph{software} libre para editores}

Las metodologías más populares en la edición contemporánea han sido
acaparadas por dos grandes empresas y sus paqueterías de
\emph{software}: Microsoft Office y Adobe Creative Cloud. Esto
representa una serie de problemas de índole económica, social, política
así como de producción y reproducción de la cultura y del conocimiento.
En este módulo se verán las posibilidades, límites, oportunidades y
riesgos de esta dependencia tecnológica o de sus alternativas: el
\emph{software} libre o el código abierto. Una vez expuesta la
problemática entre el \emph{software} libre y propietario en la edición,
se instalarán alternativas gratuitas, abiertas o libres de los programas
vendidos por Microsoft o Adobe.

\subsection*{12. Scribus y tipografías libres I}

No todo es InDesign. En el enfoque del \emph{desktop publishing} también
existe una alternativa gratuita y libre para la maquetación de textos:
Scribus. En este módulo se describirán las ventajas y desventajas de su
uso para la producción de publicaciones en relación con InDesign. Una
parte importante de este enfoque es el empleo diverso de fuentes, así
que también se abarcará la problemática entre las tipografías de pago y
las libres. A través de ejercicios simples se enseñará a instalar
Scribus, a buscar fuentes libres y a producir publicaciones de alta
calidad sin la necesidad de amplios recursos económicos o sin el
ejercicio de la piratería, al mismo tiempo que se ahorra espacio en el
disco duro y consumo de memoria \textsc{ram}.

\subsection*{13. Propiedad intelectual y derechos de autor}

En el campo de la producción cultural contemporánea se tiene un conjunto
de legislaciones cuyo discurso busca proteger la producción como
propiedad. Por ejemplo, las patentes, las marcas, el diseño industrial,
las denominaciones de origen y, por supuesto, los derechos de autor.
Pese a su complejidad jurídica y paulatino robustecimiento cuantitativo
y cualitativo, estas legislaciones ---que en conjunto se conocen como
«propiedad intelectual»--- no tienen teorías congruentes que las
justifiquen. En este módulo se analizarán las propuestas teóricas para
la protección de este régimen de propiedad, con especial énfasis en los
derechos de autor.

\subsection*{14. Bienes comunes y \emph{anticopyright}}

Ante el engrosamiento de las legislaciones relativas a la propiedad
intelectual, varios académicos y activistas han puesto énfasis en tratar
las producciones culturales como bienes comunes. En este módulo se
analizarán las críticas, las propuestas y los límites discursivos
expuestos por grupos u organizaciones a favor de los bienes comunes,
como son los movimientos del \emph{software} y la cultura libres, las
iniciativas del código y el acceso abiertos y otras posturas críticas.
La reflexión se llevará a cabo a través de los conceptos de
\emph{copyright}, \emph{copyleft}, \emph{copyjustright},
\emph{copyfarleft} y \emph{anticopyright}.

\subsection*{15. Licencias de uso para publicaciones}

Las licencias de uso son documentos que establecen la posibilidad de
reproducción de una obra como su copia, traducción o adaptación. Este
contrato «resguarda» la integridad total o parcial del producto
cultural, ya sea una pieza musical, artística, literaria o de
\emph{software}. En este módulo se reflexionará sobre las licencias de
uso más populares y cuales son las ventajas y desventajas de sus
aplicaciones.

\subsection*{16. Bibliotecas libres: LibGen, Aaaaarg, Sci-Hub y más}

Los repositorios privados constriñen el acceso y se valen de plataformas
«especializadas» solo solventables para ciertas instituciones. En este
contexto surgen las bibliotecas libres que combaten la centralización y
el control total de los derechos de autor por parte del sector privado.
En estos espacios se almacenan textos literarios, filosóficos,
feministas, anarquistas y científicos. El trabajo de estas plataformas
es muy similar porque comparten un objetivo: la defensa del acceso
abierto. En este módulo se hará un recorrido sobre el surgimiento de
estas librerías y se conocerán los parámetros necesarios para subir o
descargar obras.

\subsection*{17. Instalación virtual de \textsc{gnu}/Linux}

El uso de \textsc{gnu}/Linux hoy en día es muy sencillo. Sin embargo, por
diversos motivos no es fácil la decisión de instalar un nuevo sistema
operativo. Debido a fines pedagógicos a veces la mejor opción es el uso
de un mismo entorno. En este módulo se instalará una distribución
virtual de \textsc{gnu}/Linux sobre el sistema operativo de base; tan
simple como instalar otro programa en computadoras con Windows o macOS.
La ventaja es el acceso a las herramientas utilizadas en esta currícula
para que el usuario se enfoque en los objetivos de aprendizaje y en el
descubrimiento de las ventajas que ofrece \textsc{gnu}/Linux.

\subsection*{18. Bash: introducción al uso de la terminal}

La producción de publicaciones y la gestión de proyectos editoriales por
lo general se dan a través de interfaces gráficas. Pese a su facilidad
de uso, esto provoca inconvenientes que afectan la calidad de los
productos editoriales. El empleo de la terminal ---es decir, la ausencia
de entornos gráficos--- permite tener un mayor control sobre los
documentos y los flujos de trabajo. En este módulo se familiarizará al
usuario con la terminal Bash y sus funciones más sencillas para tener un
mayor control sobre los proyectos.

\subsection*{19. Fundamentos de programación con Ruby}

«La edición se hizo código». Esta frase, aunque abrupta, sintetiza una
cuestión rara vez visible en los procesos editoriales: no importan los
formatos empleados para la producción de publicaciones ni los programas
usados para generarlos, al final todo se reduce a ceros y unos. Con el
uso de Ruby, un lenguaje de programación, se verán los fundamentos para
empezar a desarrollar rutinas mediante \emph{scripts} que permitan la
simplificación y la automatización de tareas monótonas llevadas a cabo
en la edición.

\subsection*{20. Fundamentos de RegEx}

Aquí el texto es una cadena de caracteres. RegEx es un lenguaje y una
herramienta muy poderosa que se puede implementar en la edición si se le
usa de manera adecuada. Las expresiones regulares pueden ser muy útiles
para extraer información a través de patrones. En este módulo se
aprenderá a limpiar un texto y a darle la estructura correcta. Se
mostrarán fórmulas básicas que ayuden a sustituir información y a
entender que el «buscar» y «reemplazar» puede ofrecer una solución
confiable si se tienen las bases para utilizarlos.

\subsection*{21. Diseño \textsc{css} para publicaciones digitales}

Las hojas de estilos \textsc{css} dan pauta a un mejor diseño de las
páginas \textsc{html}. Estas nos ayudan a modificar colores, tamaños,
fuentes, fondos o sombras y la posición de distintos elementos. Este
módulo se enfocará en el desarrollo de una hoja de estilos personalizada
e indispensable para la obtención de una publicación electrónica
equilibrada y agradable a la vista. Además, se enseñarán sus ventajas y
desventajas, sus partes básicas y su declaración en el archivo
\textsc{html}.

\subsection*{22. Respositorios Git: respaldo de información}

El respaldo de la información siempre ha sido un dolor de cabeza para
los editores. Con mil y un discos duros se cree tener una solución, pero
no son confiables cuando son demasiados colaboradores. Una solución es
Git, un programa de código abierto que realiza un seguimiento preciso de
los cambios en un proyecto, más conocido en tecnologías de la
información como un controlador de versiones. Este módulo brindará las
herramientas y los conocimientos necesarios para utilizar repositorios
Git en lugar de discos duros, nubes ---como Google Drive o Dropbox--- o
sin fines de versiones últimas de cada proyecto editorial. Se enseñará
la configuración de un repositorio y su mantenimiento, la subida y
descarga de información y la adición de colaboradores: todo de manera
gratuita.

