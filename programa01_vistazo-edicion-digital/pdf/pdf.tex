% universal settings
\documentclass[a5paper,11pt,oneside,onecolumn,openright,extrafontsizes]{memoir}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}
\usepackage[osf]{Alegreya,AlegreyaSans}

% PACKAGE DEFINITION
% typographical packages
\usepackage{microtype} % for micro-typographical adjustments
\usepackage{setspace} % for line spacing
\usepackage{lettrine} % for drop caps and awesome chapter beginnings
\usepackage{titlesec} % for manipulation of chapter titles

% for placeholder text012180004614994322
\usepackage{lipsum} % to generate Lorem Ipsum

% other
\usepackage{calc}
\usepackage{hologo}
\usepackage[pdfencoding=auto]{hyperref}
%\usepackage{showframe}

% PHYSICAL DOCUMENT SETUP
% media settings
\setstocksize{8.5in}{5.675in}
\settrimmedsize{8.5in}{5.5in}{*}
\setbinding{0.175in}
\setlrmarginsandblock{0.611in}{1.222in}{*}
\setulmarginsandblock{0.722in}{1.545in}{*}

% defining the title and the author
%\title{\LaTeX{} ePub Template}
%\title{\textsc{how i started to love {\fontfamily{cmr}\selectfont\LaTeX{}}}}
\title{La edición \\ se hizo código}
\author{
  Melisa Bayardo \vskip 1em
  Ramiro Santa Ana Anguiano
}

% custom second title page
\makeatletter
\newcommand*\halftitlepage{\begingroup % Misericords, T&H p 153
  \setlength\drop{0.1\textheight}
  \begin{center}
  \vspace*{\drop}
  \rule{\textwidth}{0in}\par
  {\large\textsc\thetitle\par}
  \rule{\textwidth}{0in}\par
  \vfill
  \end{center}
\endgroup}
\makeatother

% custom title page
\thispagestyle{empty}
\makeatletter
\newlength\drop
\newcommand*\titleM{\begingroup % Misericords, T&H p 153
  \setlength\drop{0.15\textheight}
  \begin{center}
  \vspace*{\drop}
  \rule{\textwidth}{0in}\par
  {\HUGE\textsc\thetitle \vskip .5em \Large Taller intensivo de edición digital\par}
  \rule{\textwidth}{0in}\par
  \vskip 2em
  {\Large\textit\theauthor\par}
  \end{center}
\endgroup}
\makeatother

% chapter title display
\titleformat
{\chapter}
[display]
{\normalfont\scshape\huge}
{\HUGE\thechapter\centering}
{0pt}
{\vspace{18pt}\centering}[\vspace{42pt}]

% typographical settings for the body text
\setlength{\parskip}{0em}
\linespread{1.09}

% HEADER AND FOOTER MANIPULATION
  % for normal pages
  \nouppercaseheads
  \headsep = 0.16in
  \makepagestyle{mystyle} 
  \setlength{\headwidth}{\dimexpr\textwidth+\marginparsep+\marginparwidth\relax}
  \makerunningwidth{mystyle}{\headwidth}
  \makeevenhead{mystyle}{}{\textsf{\scriptsize\scshape\thetitle}}{}
  \makeoddhead{mystyle}{}{\textsf{\scriptsize\scshape La edición se hizo código}}{}
  \makeevenfoot{mystyle}{}{}{}
  \makeoddfoot{mystyle}{}{\textsf{\scriptsize\thepage}}{}
  \makeatletter
  \makepsmarks{mystyle}{%
  \createmark{chapter}{left}{nonumber}{\@chapapp\ }{.\ }}
  \makeatother
  % for pages where chapters begin
  \makepagestyle{mystyle}
  \makerunningwidth{mystyle}{\headwidth}
  \makeevenhead{mystyle}{}{\textsf{\scriptsize\scshape\thetitle}}{}
  \makeoddhead{mystyle}{}{\textsf{\scriptsize\scshape La edición se hizo código}}{}
  \makeevenfoot{mystyle}{}{\textsf{\scriptsize\thepage}}{}
  \makeoddfoot{mystyle}{}{\textsf{\scriptsize\thepage}}{}
  \pagestyle{mystyle}
% END HEADER AND FOOTER MANIPULATION

% table of contents customisation
\renewcommand\cftchapterfont{\normalfont}
\renewcommand{\cftchapterpagefont}{\normalfont}
\renewcommand{\printtoctitle}{\centering\huge\scshape}

% layout check and fix
\checkandfixthelayout
\fixpdflayout

% BEGIN THE DOCUMENT
\begin{document}
\pagestyle{empty}
% the title page
\titleM

% the TOC
\pagestyle{mystyle}
\frontmatter
\clearpage
\renewcommand\contentsname{Índice}
\tableofcontents*

\mainmatter

\chapter*{Descripción}
\addcontentsline{toc}{chapter}{Descripción}

\noindent La edición es ya edición digital. ¿Usas una computadora para tus libros
impresos o digitales? ¿La imprenta a la que acudes solo te recibe
formatos digitales? El uso de computadoras y archivos digitales no es
una primicia en la edición. La discusión entre las publicaciones en
papel y las electrónicas ya no es una novedad.

Sin embargo, la tradición editorial ha ignorado que \emph{no es un
cambio de técnicas ni de dispositivos sino la pérdida de los fundamentos
básicos de la edición}:

\begin{itemize}
\tightlist
\item
  La edición no solo es una profesión, un arte o una tradición, también
  es un método y una pedagogía.
\item
  El texto no solo es diseño y contenido, asimismo es estructura.
\item
  Las publicaciones ya no solo se editan y diseñan, del mismo modo se
  programan.
\end{itemize}

\section*{Antecedentes}
\addcontentsline{toc}{section}{Antecedentes}

\noindent El quehacer editorial se ha valido de un conjunto de saberes,
tecnologías y técnicas en pos de una mayor calidad en el menor tiempo
posible. Desde el nacimiento de la imprenta hasta antes del surgimiento
de las tecnologías digitales es posible rastrear una línea evolutiva.

El objetivo era claro: publicar de la manera más eficiente y eficaz
posible. Sin embargo, este ideal se regía por un simple supuesto: la
obra siempre se fijaba en un solo formato.

La publicación era la obra hecha papel. La sinonimia era tal que no
había una distinción clara entre los conceptos de «obra», «libro»,
«soporte» y «formato». La relación era tan estrecha que la definición de
cada concepto siempre implicaba las nociones de «diseño», «contenido»,
«papel» y «página».

Sin conciencia clara, el surgimiento de las nuevas tecnologías de la
información y la comunicación dinamitaron el supuesto que regía al
quehacer editorial. En tan solo una década la idea de obra se
diversificó a su concreción en diversos formatos y soportes. El papel y
la página dejaron de ser características definitorias del trabajo
editorial. El diseño y el cuidado del contenido comenzaron a
subordinarse a las estructuras textuales que incluso hace posible
prescindir de la escritura.

Como editores, ¿qué nos queda y dónde nos ubicamos cuando los elementos
clave de la edición son ya características secundarias o llanas
metáforas?

\section*{Proyección}
\addcontentsline{toc}{section}{Proyección}

\noindent La edición como método siempre ha sido parte de la tradición. La
cuestión estriba en que por lo general fue confundida por la
experiencia, la intuición, la creatividad y la genialidad de cada
persona dedicada a los procesos editoriales.

Cuando la producción de un objeto implica una serie de procesos; cuando
estos procesos llegan a ser tan monótonos y maquinales, en realidad de
lo que hablamos es de un método. Cuando este método se desconoce y de
repente se ha de aprender; cuando las habilidades editoriales se afinan
en el trabajo mismo, lo que se está realizando es un proceso pedagógico.

El objetivo de la edición se vuelve más complejo: publicar multiformato
de la manera más eficaz y eficiente posible. Esto acarrea un nuevo
supuesto: la edición tiene que automatizarse. Pero para ello se requiere
una redefinición de la edición como método y pedagogía de producción.

Este taller intensivo es una introducción a esta nueva manera de pensar
la edición.

\chapter*{Información general}
\addcontentsline{toc}{chapter}{Información general}

\noindent El taller se planea realizarse en diez sesiones intensivas con un total
de veinticinco horas. Las primeras cinco sesiones serían de dos horas y
las restantes de tres.

\section*{Objetivos}
\addcontentsline{toc}{section}{Objetivos}

\noindent El taller intensivo pretende dotar de los elementos mínimos necesarios
para una producción más eficiente de publicaciones. Aunque en la
actualidad la metodología propuesta responde de mejor manera a formatos
digitales, también es aplicable a medios impresos.

\subsection*{General}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Introducir al asistente a la publicación «desde cero», multiformato y
  automatizada con \emph{software} libre o de código abierto.
\end{enumerate}

\subsection*{Particulares}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Visualizar los problemas metodológicos entre ecosistemas editoriales
  cerrados y la necesidad multiformato.
\item
  Aprender metodologías ramificadas de producción editorial.
\item
  Explicar la pertinencia de un árbol de directorios consistente.
\item
  Usar formatos abiertos para explicitar su versatilidad.
\item
  Practicar uso de la terminal como método de empoderamiento
  tecnológico.
\item
  Explotar tecnologías abiertas para la ejecución y mantenimiento de
  proyectos.
\item
  Experimentar en el retorno a ecosistemas editoriales cerrados u otros
  caminos para la producción de impresos.
\end{enumerate}

\section*{Propuesta de cronograma}
\addcontentsline{toc}{section}{Propuesta de cronograma}

\noindent Todas las sesiones serían entresemana de nueve a once de la mañana para
las primeras cinco sesiones y de nueve a doce para las últimas. Por la
cantidad de actividades a realizar, se propone el siguiente horario:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Sesión 1: 22 de octubre (lunes).
\item
  Sesión 2: 23 de octubre (martes).
\item
  Sesión 3: 24 de octubre (miércoles).
\item
  Sesión 4: 25 de octubre (jueves).
\item
  Sesión 5: 29 de octubre (lunes).
\item
  Sesión 6: 30 de octubre (martes).
\item
  Sesión 7: 5 de noviembre (lunes).
\item
  Sesión 8: 6 de noviembre (martes).
\item
  Sesión 9: 7 de noviembre (miércoles).
\item
  Sesión 10: 8 de noviembre (jueves).
\end{enumerate}

\section*{Requisitos previos}
\addcontentsline{toc}{section}{Requisitos previos}

\noindent Para una gestación fluida del taller, es recomendable que los asistentes
cuenten con lo siguiente:

\begin{itemize}
\tightlist
\item
  Un equipo de cómputo, se recomienda sistemas \textsc{unix} (macOS o
  Linux).
\item
  Conocimientos generales de procesos editoriales tradicionales.
\item
  Conocimientos sobre cómo instalar programas.
\item
  Idea básica de estructuras \textsc{html}.
\end{itemize}

\section*{Necesidades espaciales}
\addcontentsline{toc}{section}{Necesidades espaciales}

\noindent El taller supone que se realizará en un espacio que tiene lo siguiente:

\begin{itemize}
\tightlist
\item
  Conexión a internet.
\item
  Proyector o pintarrón.
\end{itemize}

\chapter*{\emph{Syllabus}}
\addcontentsline{toc}{chapter}{\emph{Syllabus}}

\section*{Sesión 1: Más allá de Adobe}
\addcontentsline{toc}{section}{Sesión 1: Más allá de Adobe}

\noindent Adobe ha mutado de una \emph{suite} de diseño a una metodología única de
producción editorial. Al parecer Adobe lo tiene todo, su uso es
demandado por el sector editorial y su aprendizaje brota desde las
universidades hasta seminarios y talleres profesionales. Pero tiene
costos ocultos. El más grave es la reducción de una profesión y un
método al aprendizaje basado en el consumo de \emph{software}. ¿Qué
pasará si un día Adobe tiene la misma fortuna que QuarkXPress o
PageMaker?

Esta primera sesión comenzará con una confrontación cara a cara con el
formateo del texto creado en ecosistemas cerrados.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Exportación a \textsc{html} desde InDesign.
\item
  Análisis de estructuras textuales y hojas de estilo.
\item
  Demostración de la necesidad de limpieza del formato.
\item
  Introducción a RegEx como vía para la limpieza del formato.
\item
  Instalación de \emph{software} necesario para el taller.
\end{enumerate}

\section*{Sesión 2: Bienvenido a la edición \\ «desde cero»}
\addcontentsline{toc}{section}{Sesión 2: Bienvenido a la edición «desde cero»}

\noindent En la periferia del ecosistema de Adobe nos encontramos con una
pluralidad de programas y métodos. No siempre son compatibles entre sí.
En la mayoría de los casos esta diversidad deviene en frustración.

En esta sesión se iniciará el traslado del aprendizaje basado en
programas a uno fundado en métodos, empezando por la manera adecuada de
limpiar el formato de un texto.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Aprendizaje de los elementos comunes de RegEx para la limpieza de
  un texto.
\item
  Uso de RegEx en el texto de muestra.
\end{enumerate}

\section*{Sesión 3: ¡Esto es Pecas! \textgreater{} Herramientas editoriales libres}
\addcontentsline{toc}{section}{Sesión 3: ¡Esto es Pecas! \textgreater{} Herramientas editoriales libres}

\noindent Gracias a estas herramientas hacer un \textsc{epub} nunca había sido tan
fácil como lo es ahora. En un entorno de código y según el método
ramificado de edición es como surgieron estas increíbles herramientas
editoriales. Una serie de \emph{scripts} automatizados pueden llegar a
hacer la diferencia si se utilizan de manera correcta.

En este primer acercamiento nos interesa trabajar algunos párametros
básicos que son parte de nuestro proyecto editorial. Así podremos formar
nuestro primer \textsc{epub}.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  ¿Qué es Pecas?
\item
  Revisión de párametros (repaso sencillo).
\item
  ¿Qué es el archivo \textsc{yaml} y cómo es su sintaxis?
\item
  Uso de bash.
\end{enumerate}

\section*{Sesión 4: De la planicie y los tornados al bosque y las ramas}
\addcontentsline{toc}{section}{Sesión 4: De la planicie y los tornados al bosque y las ramas}

\noindent Tiene que haber una mejor manera, ¿cierto? El formateo de texto es el
cuello de botella de la edición. No hay mejor manera de visualizarlo que
haber sufrido en la limpieza del texto. Esto abre la puerta para
teorizar en dos orientaciones metodológicas para producción
estandarizada de publicaciones: la edición cíclica y la edición
ramificada.

En esta sesión se teorizará sobre las ventajas y límites de cada
orientación metodológica para la edición.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Bases generales de la edición digital.
\item
  Tipos de edición digital.
\item
  Tipos de edición estandarizada.
\item
  Edición tradicional.
\item
  Edición cíclica.012180004614994322
\item
  Edición ramificada.
\item
  Ejemplos de edición ramificada.
\end{enumerate}

\section*{Sesión 5: Bienvenidos al mundo de Markdown}
\addcontentsline{toc}{section}{Sesión 5: Bienvenidos al mundo de Markdown}

\noindent Olvidémonos de las etiquetas \textsc{html} y conozcamos Markdown, un
lenguaje de marcado ligero que nos facilita la escritura en texto plano.
Esta herramienta aparte de tener una sintaxis sencilla, nos brinda
control en la estructura y facilita la atribución de los estilos. Ya no
es necesario pasar del Word al código; con este lenguaje ¡lo tenemos
todo!

En esta sesión aprenderemos a crear nuestro propio árbol de directorios,
conoceremos su eficacia y el por qué son tan importantes. Veremos con
esto la creación de nuestro primer documento \texttt{.md} y la sintaxis
que requiere.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  ¿Qué es un árbol de directorios?
\item
  Moviéndonos entre carpetas.
\item
  Rutas relativas.
\item
  ¿Cuáles son los enfoques \textsc{wysiwyg} y \textsc{wysiwym}?
\item
  Introducción a Markdown.
\item
  Sintaxis de Markdown.
\end{enumerate}

\section*{Sesión 6: Nuestro primer proyecto con Markdown}
\addcontentsline{toc}{section}{Sesión 6: Nuestro primer proyecto con Markdown}

\noindent Ya vimos que Markdown es un diamante en bruto, ahora solo nos falta
ponerlo a prueba y construir nuestro propio contenido con sintaxis de
Markdown.

En esta etapa de seguimiento haremos nuestra primera publicación con
Markdown, navegaremos entre carpetas y conoceremos el árbol de
directorios a través de las rutas relativas.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  ¿Cómo trabajar con Markdown desde editores de texto?
\item
  Uso del formato \textsc{md}.
\item
  Uso de clases e identificadores con Markdown.
\item
  Uso de clases por defecto de Pecas Markdown.
\end{enumerate}

\section*{Sesión 7: ¿Otra vez Pecas?}
\addcontentsline{toc}{section}{Sesión 7: ¿Otra vez Pecas?}

\noindent Es cierto que ya vimos estas herramientas editoriales, pero, ¿qué tan
claro ha quedado su funcionamiento? Es importante conocer cuáles son los
parámetros obligatorios u opcionales para su buen desempeño. Con esto
veremos cada una de las funcionalidades y lo que podemos hacer con
ellas.

En este apartado conoceremos a fondo cómo utilizar Pecas y cómo podemos
combinar los parámetros según las necesidades de cada proyecto
editorial.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Comandos de bash.
\item
  Herramientas para trabajar con nuestros archivos madre:
  \texttt{pc-add}, \texttt{pc-analytics}, \texttt{pc-images} y
  \texttt{pc-pandog}.
\item
  Herramientas para \textsc{epub}: \texttt{pc-automata},
  \texttt{pc-creator}, \texttt{pc-divider}, \texttt{pc-notes},
  \texttt{pc-cites}, \texttt{pc-index}, \texttt{pc-recreator} y
  \texttt{pc-changer}.
\item
  Verificación de \textsc{epub}: \texttt{epubcheck} 3 y 4.
\item
  Estado de Pecas y sus dependencias: \texttt{pc-doctor}.
\end{enumerate}

\section*{Sesión 8: Mi primer \textsc{epub}}
\addcontentsline{toc}{section}{Sesión 8: Mi primer \textsc{epub}}

\noindent Un \textsc{epub} es una serie de archivos \textsc{xhtml} que son comprimidos
en un archivo \textsc{zip} para su portabilidad y su fácil lectura.
Nosotros ya nos enfocamos en hacer nuestro contenido y lo que conlleva
crear una publicación. Solo nos falta conocer el proceso para generar
nuestro \textsc{epub} en versiones 3.0.1, 3.0.0 y el \textsc{mobi} para
Kindle.

En esta sesión veremos cómo comprimir un \textsc{epub} desde la terminal
utilizando Pecas.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  ¿Cómo comprimir un \textsc{epub}?
\item
  ¿Qué parámetros vamos a emplear para nuestra publicación?
\item
  ¿De qué nos sirve la analítica?
\item
  Verificación de \textsc{epub} con EpubCheck 3 y 4.
\end{enumerate}

\section*{Sesión 9: Viaje a la exósfera}
\addcontentsline{toc}{section}{Sesión 9: Viaje a la exósfera}

\noindent En el desarrollo de un proyecto tenemos necesidad de respaldar la
información. Cuando concluimos también precisamos archivarlo. ¿Cuántas
veces se ha vuelto un dolor de cabeza el uso de discos duros o de nubes?
¿Cuántas veces no nos hemos confundido y hemos trabajado doble o, peor
aún, se ha perdido información?

En este sesión trataremos otro método para poder tener un control sobre
nuestro proyecto, en el que los discos duros y las nubes se vuelven
innecesarios.

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  ¿Qué es \texttt{git}? ¿Qué es un proyecto como repositorio remoto?
\item
  Creación de cuentas para el manejo de repositorios remotos.
\item
  Creación de llaves de acceso.
\item
  Creación de equipos de trabajo.
\item
  Creación de repositorios.
\item
  Clonación local de repositorios.
\item
  Dinámica de \texttt{pull} y \texttt{push}.
\item
  Visualización de estados e historial de repositorios.
\end{enumerate}

\section*{Sesión 10: Regreso a la tierra}
\addcontentsline{toc}{section}{Sesión 10: Regreso a la tierra}

\noindent ¿Quieres un impreso? La edición ramificada vista aquí no necesariamente
implica una apertura en los ecosistemas editoriales. Es posible regresar
a ecosistemas cerrados para crear un \textsc{pdf} para impresión, aunque
también desde hace más de treinta años existe otra vía\ldots{}

\subsection*{Temas}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Conversión de \textsc{md} a \textsc{xml}.
\item
  Importación de \textsc{xml} a InDesign.
\item
  Asociación de etiquetas \textsc{xml} con estilos de párrafo o de
  caracteres.
\item
  ¿Qué es \TeX? ¿Qué es \LaTeX?
\item
  Conversión de \textsc{md} a \TeX.
\item
  Aplicación de plantillas de \LaTeX.
\item
  Creación de \textsc{pdf} con \LaTeX.
\end{enumerate}

\chapter*{Costos}
\addcontentsline{toc}{chapter}{Costos}

\noindent Las veinticinco horas de taller más una memoria en video tienen un costo
de \$25,000.00 pesos más \textsc{iva}.

Se requiere un anticipo del 50\% y lo restante para más tardar la última
sesión del taller.

El taller no tiene cupo limitado, aunque para una mejor experiencia se
recomienda que no sea mayor a diez asistentes.

El precio ya incluye toda la infraestructura y materiales necesarios
para el taller y la producción de la memoria.

La memoria será propiedad de los asistentes, los talleristas no pondrán
restricciones sobre su uso.

\end{document}
% END THE DOCUMENT
